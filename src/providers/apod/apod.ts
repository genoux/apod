import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';
/*
  Generated class for the WebhoseioProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApodProvider {
  public dataStorage = [];

  constructor(private http: Http) {
  }

getDataAPI(url){
 return this.http.get(url)
 .do(this.logResponse)
 .map(this.extractData)
 .catch(this.catchError);
}
private catchError(error : Response | any){
console.log(error)
return Observable.throw(error.json() || "Server error.");
}

private logResponse(res: Response){

}
private extractData(res :Response){

let body = res.text();  // If response is a JSON use json()
if (body) {
    return res.json();
 } else {
    return {};
 }
}

}