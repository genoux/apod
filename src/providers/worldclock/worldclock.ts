import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';
import { format, subDays , getDate, getMonth , getTime, parse, startOfTomorrow,isEqual, addMinutes , addSeconds , isToday, isTomorrow} from 'date-fns'

/*
  Generated class for the WorldclockProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class WorldclockProvider {
currentTime;
currentDate;
tomorrowDate;

testTomorrowDate;
resultTest;


  constructor(private http: Http) {
    this.tomorrowDate = startOfTomorrow();
    //console.log('startOfTomorrow();', startOfTomorrow());
  }

  filetime_to_unixtime(ft) { 
    if(this.testTomorrowDate == null){
      this.testTomorrowDate = addMinutes(new Date(ft), 1);
     // console.log('Next Date: ',  this.testTomorrowDate);
    }


    this.resultTest = new Date(ft);
    var result = parse(ft);

    this.currentTime = format(result, 'h:mm A') ;
    this.currentDate = format(result, 'dddd Do MMMM YYYY');


    return result;
  }
  

  

  getTime(){
    return this.http.get("http://worldclockapi.com/api/json/utc/now")
    .do(this.logResponse)
    .map(this.extractData)
    .catch(this.catchError);
   }
   private catchError(error : Response | any){
   console.log(error)
   return Observable.throw(error.json() || "Server error.");
   }
   
   private logResponse(res: Response){
   }
   private extractData(res :Response){
   return res.json();
   }

}
