import {
  HttpClient
} from '@angular/common/http';
import {
  Injectable
} from '@angular/core';
import {
  AngularFireDatabase,AngularFireList
} from 'angularfire2/database';
import firebase from 'firebase';
import _ from 'lodash';

/*Generated class for the FirebaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class FirebaseProvider {
  
public apodDatabase = []
public apodFetch: AngularFireList<any[]>;
  constructor(public afd: AngularFireDatabase) {
    this.apodFetch = this.afd.list('/data'); 

  }

  /*updateDatabase(data) {
    const apod: firebase.database.Reference = firebase.database().ref("/data/");
    apod.child(data[0].date).update({
      title: data[0].title,
      explanation: data[0].explanation,
      date: data[0].date,
      url: data[0].url
    })
  }*/
 /* setDatabase(data) {
    var object = _.sortBy(data, "date");
    object.reverse();
    const apod: firebase.database.Reference = firebase.database().ref("/data/");
    for (var i = 0; i < object.length; i++) {
      apod.child(data[i].date).set({
        title: object[i].title,
        explanation: object[i].explanation,
        date: object[i].date,
        url: object[i].url
      })
    }
  }*/
 /* pushDatabase(data) {
    console.log('Pushing some data', data);
    const apod: firebase.database.Reference = firebase.database().ref("/data/");
    apod.child(data[0].date).push({
      title: data[0].title,
      explanation: data[0].explanation,
      date: data[0].date,
      url: data[0].url
    })
  }*/

  getDatabase() {
      console.log('Fetching data base...');
      return this.apodFetch;
  
  }
}
