import { BrowserModule } from '@angular/platform-browser';

import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule, IonicPageModule } from 'ionic-angular';

import {HttpModule} from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';


import { Observable } from 'rxjs/Observable';

import { WebhoseioProvider } from '../providers/webhoseio/webhoseio';
import { FirebaseProvider } from '../providers/firebase/firebase';
import { ApodProvider } from '../providers/apod/apod';

// for AngularFireDatabase
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule, AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';

import {RoundProgressModule, RoundProgressConfig} from 'angular-svg-round-progressbar';
import { AppVersion } from '@ionic-native/app-version';

import { WorldclockProvider } from '../providers/worldclock/worldclock';
import { SuperTabsModule } from 'ionic2-super-tabs';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { PhotoViewer } from '@ionic-native/photo-viewer';
import { IonicImageViewerModule } from 'ionic-img-viewer';
import { IonicStorageModule } from '@ionic/storage';
import { DatePicker } from '@ionic-native/date-picker';
import { ParallaxModule } from 'ionic-parallax';
// Initialize Firebase
var firebaseConfig = {
    apiKey: "AIzaSyBu1J_fPfUjv9kHUvR3sGj6wVbvP3BjoXI",
    authDomain: "earth-is-feeling.firebaseapp.com",
    databaseURL: "https://earth-is-feeling.firebaseio.com",
    projectId: "earth-is-feeling",
    storageBucket: "",
    messagingSenderId: "882190827406"
  };



@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    HttpClientModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    SuperTabsModule.forRoot(),
    RoundProgressModule,
    BrowserAnimationsModule,
    IonicImageViewerModule,
    IonicStorageModule.forRoot(),
    ParallaxModule
    
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler },
    WebhoseioProvider,
    FirebaseProvider,
    WorldclockProvider,
    AppVersion,
    ApodProvider,
    PhotoViewer,
    DatePicker
  ],

})
export class AppModule {}
