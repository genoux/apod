import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Slides } from 'ionic-angular';
/**
 * Generated class for the InfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-info',
  templateUrl: 'info.html',
})
export class InfoPage {

apod_description;
apod_title;
getApod;
currentIndex;
currentImg;
textArray = [];
  constructor( public navCtrl: NavController, public navParams: NavParams ,public viewCtrl: ViewController) {
    this.getApod = navParams.get('apod');
    this.currentIndex = navParams.get('currentIndex');
    this.currentImg = navParams.get('currentImg');
    this.getExplanation();
  }

  getExplanation(){
    for(var i =0; i< this.getApod.length; i++){
      var obj = this.getApod[i];
      for(var a =0 ; a < obj.length; a++){      
        if(a == this.currentIndex){
          var explanationData = obj[a]['explanation'];
          var setArray = explanationData.split('.');
          this.textArray = setArray.filter(value => Object.keys(value).length !== 0);
          this.apod_title= obj[a]['title'];
        }
      }
    }
  }
  ionViewDidEnter() {}
  closeModal() {
    this.viewCtrl.dismiss();
  }
}
