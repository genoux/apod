import {Component, trigger, state, style, animate, transition, keyframes } from '@angular/core';
import {NavController, NavParams, ModalController, IonicPage, ActionSheetController, AlertController,Platform } from 'ionic-angular';
import {WebhoseioProvider} from '../../providers/webhoseio/webhoseio';
import {ApodProvider} from '../../providers/apod/apod';
import {WorldclockProvider} from '../../providers/worldclock/worldclock';
import {HttpModule} from '@angular/http';
import {FirebaseProvider} from '../../providers/firebase/firebase';
import {AngularFireList} from 'angularfire2/database';
import firebase from 'firebase';
import {RoundProgressModule, RoundProgressConfig} from 'angular-svg-round-progressbar';
import {AppVersion} from '@ionic-native/app-version';
import _ from 'lodash';
import { Directive, ElementRef } from '@angular/core';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { IonicImageViewerModule } from 'ionic-img-viewer';
import { ImageViewerController } from 'ionic-img-viewer';
import { ImageViewer } from 'ionic-img-viewer/dist/es2015/src/image-viewer';
import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { SplashScreen } from '@ionic-native/splash-screen';
import { DatePicker } from '@ionic-native/date-picker';
import { format, subDays , getDate, getMonth , getTime, parse, startOfTomorrow,isEqual, addMinutes , addSeconds , isToday, isTomorrow} from 'date-fns'
import { PopoverController } from 'ionic-angular';
import { timer } from 'rxjs/observable/timer';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})

export class HomePage {
  @ViewChild(Slides) slides: Slides;
  _imageViewerCtrl: ImageViewerController;
  public fetchFirebase = []
  showSplash = true;
  bookmarkToggle: string = "bookOff";
  bookmarkIndex;
  currentImg;
  currentTitle;

  currentIndex;
  animatedSlide = "slideAnim";

  lastDate;
  dateValue  
  today = format(new Date(), 'YYYY-MM-DD');


  slideElm;
  slidingValue = 0;
  currSlidePos;
  savedSlidePos = -0;
  constructor(public popoverCtrl: PopoverController, private platform: Platform, private datePicker: DatePicker, private splashScreen: SplashScreen, private storage: Storage, private alertCtrl: AlertController, public actionSheetCtrl: ActionSheetController,private photoViewer: PhotoViewer,private imageViewerCtrl: ImageViewerController, private appVersion: AppVersion, private _config: RoundProgressConfig, public navCtrl:NavController, public navParams:NavParams, public modalCtrl: ModalController, private apodProvider: ApodProvider, public firebaseProvider: FirebaseProvider) {

    this.getDatabase();
    this._imageViewerCtrl = imageViewerCtrl;
    this.currentIndex = 0;
    storage.get('book-key').then((val) => {
      this.bookmarkIndex = val
    });
  }

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create("MenuPage", {}, {
      showBackdrop: true
  });
    popover.present({
      ev: myEvent
    });
  }

  /*---------------------------------------- Open info page */
  /*openModal() {
    this.navCtrl.push("InfoPage");
  }*/

/*---------------------------------------- When refreshing page */
doRefresh(refresher) {
  setTimeout(() => {
    console.log('Async operation has ended');
    refresher.complete();
    this.getDatabase();
    this.goToSlide(0);
  }, 2000);
}

/*---------------------------------------- Remove splash screen */
ionViewDidEnter() {
  this.platform.ready().then(() => {
    timer(3000).subscribe(()=> {
      this.showSplash = false
      this.setSlide();
    })
  });
}

confirmDate(){
  for(var i =0; i< this.fetchFirebase.length; i++){
    var obj = this.fetchFirebase[i];
    for(var a = 0; a < obj.length;a++){
      if(obj[a]["date"] == this.dateValue){       
        this.goToSlide(a);
      }
    }
  }
}
  
/*---------------------------------------- Read the database */
getDatabase(){
  this.firebaseProvider.getDatabase().valueChanges().subscribe(x => {
    this.fetchFirebase = []
    var object = _.sortBy(x, "date");
    object.reverse();
    this.fetchFirebase.push(object);
    let getLastDate = object.slice(-1).pop()["date"]   
    this.lastDate = getLastDate;  
    this.currentImg = object[0]['url'];
    this.currentTitle =  object[0]['title']
  });
}

/*---------------------------------------- Toggle bookmark */
  setBookmark(getIcon: string) {
    if(this.bookmarkIndex == null){
      //Book is null, set it to current slide
      this.bookmarkToggle = "bookOn";
      this.bookmarkIndex = this.slides.getActiveIndex();
    }else if(this.slides.getActiveIndex() == this.bookmarkIndex){
      //If we are on the book slide and its on, set it off - no alert
      if (this.bookmarkToggle === 'bookOn') {
          this.bookmarkToggle = "bookOff"
          this.bookmarkIndex = null;
      }
      }else{
        this.presentConfirm();
      }
    this.storage.set('book-key', this.bookmarkIndex);
    }
/*---------------------------------------- Move to first silder */
goToSlide(bm){
  this.slides.slideTo(bm, 500);
}

ionSlideDrag(centerX: number){
  this.animatedSlide=""
}

setSlide(){
  if(this.currentIndex == 0){
    this.slides.lockSwipeToPrev(true);
  }else if (this.currentIndex >=0){
    this.slides.lockSwipeToPrev(false);
  }
}
/*---------------------------------------- When the slider change */
slideChanged(a) {
  this.slidingValue = -0;
  this.savedSlidePos = this.currSlidePos;
  this.slideElm = null;
  this.currentIndex = this.slides.getActiveIndex();
  this.currentImg = a[this.currentIndex]["url"];
  this.currentTitle =  a[this.currentIndex]['title'];

  if(this.bookmarkIndex != this.slides.getActiveIndex()){
    this.bookmarkToggle = "bookOff";
  }else{
    this.bookmarkToggle = "bookOn";
  }
  if(this.currentIndex == 0){
    this.slides.lockSwipeToPrev(true);
  }else if (this.currentIndex >=0){
    this.slides.lockSwipeToPrev(false);
  }
}

/*---------------------------------------- Show image on popup */
presentImage(myImage) {
  this.photoViewer.show(myImage, this.currentTitle , {share: false});
}
/*---------------------------------------- Show Info */
presentInfo(myImage) {
  this.navCtrl.push("InfoPage", {
    apod: this.fetchFirebase,
    currentIndex: this.currentIndex,
    currentImg: this.currentImg
  });
}

/*---------------------------------------- Popup confirm */
  presentConfirm() {
    let alert = this.alertCtrl.create({
      title: 'Edit bookmark',
      message: 'You\'re about to edit bookmark!',
      buttons: [
        {
          text: 'Go to my last bookmark',
          handler: () => {
            this.goToSlide(this.bookmarkIndex);
          }
        },
        {
          text: 'Update bookmark',
          handler: () => {
            console.log('Saving bookmark');
            this.bookmarkIndex = this.slides.getActiveIndex();
            this.bookmarkToggle = "bookOn";
            this.storage.set('book-key', this.bookmarkIndex);
          }
        },
        {
          text: 'Never mind',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    alert.present();
  }
}
